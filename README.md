# Projet Web
--- 

## NOTES IMPORTANTES
---
- Avant de commencer, vous devez créer un compte sur [www.gitlab.com](https://gitlab.com/).
- Votre compte doit contenir vos noms et prénoms, et une photo qui me permettra de vous reconnaître.
- Une fois le compte créer, vous devez poster un message [ici](https://gitlab.com/M1106/projet/issues/1) (si ce n'est pas déjà fait)
- Le projet WEB tient sur 3 TPs qui sont normalement plus que suffisant pour le réaliser. 
- Regardez systématiquement et souvent les [issues](https://gitlab.com/m1106/projet/issues/) les profs et les étudiants y postent des choses pouvant être intéressantes
- Si vous voyez des erreurs dans le dépôt, si vous avez des suggestions, ouvrez un issue [ici](https://gitlab.com/m1106/projet/issues)


## Sujet
---

Vous êtes maintenant membre de la célèbre association GEEKO adossée au Département Réseaux et Télécommunications de l'IUT de Saint-Pierre.
Vous venez d’intégrer l’équipe de développement de cette asssociation. 
Vous devez créer un site web pour cette association qui n'en a pas pour le moment (QUELLE HONTE !!!).

Une première enquête a été réalisée pour recueillir les besoins de l'association. 
Voici un résumé de ce que le conseil d'administration de l'association espère.

### Fonctionnalités:

Le site Web doit contenir:
- Une page avec les objectifs de l'association
- Les status
- Le réglement intérieur
- Les membres du conseil d'administration et leur rôle avec photos.

Le site devra aussi avoir les sections suivantes:
- Les actions de l'association: le projet ZOT'COD
- Les photos de la journée d'intégration


Le site :
- doit s'adapter à plusieurs taille d'écran (tablette, smartphone, ordinateur classique) - responsive mais aussi au grossissement des caractères
- ne doit contenir aucune erreur
- doit être écrit en HTML et CSS seulement : pas de javascript ou autre
- doit s'afficher correctement sur plusieurs navigateurs dont: chrome, firefox, safari (au minimum)


Si il vous manque des documents, merci de prendre contact avec Mlle Sabrina Baco présidente actuelle de l'association.



## Consignes
---

Vous allez réaliser un site web qui devra faire appel à toutes vos connaissances acquises pendant les cours M1106 : **HTML, CSS**. **Le projet sera réalisé de manière individuelle** sur 3 séances de TP et en dehors des cours. 

**Vous n’êtes pas autorisé à utiliser de framework**. Et vous devrez être capable de justifier la présence de chaque ligne de code du projet.

## L’évaluation
---
L’évaluation du projet se fera sur la présentation orale de 5 min (dernière séance de TP) au cours de laquelle vous présenterez à votre responsable (moi) de TP (barème à titre indicatif):

1. Les pages (total: 3) 
    - Objectifs de l'association (.5)
    - Les status et réglement intérieur (.5)
    - Les membres du conseil d'administration et leur rôle avec photos. (.5)
    - Les actions de l'association: le projet ZOT'COD (.5)
    - Les photos de la journée d'intégration (1)
2. La navigation de votre site (total: 4)
    - Les menus (1)
    - Les liens entre les pages(1)
    - La facilité de navigation (1)
    - demonstration de navigation dans le site (1)
3. Le design (total: 2)
    - Le choix des couleurs (.5)
    - Le choix des polices (.5)
    - Les formats des pages (1)
3. Les fonctionnalités techniques (total: 4)
    - site sans erreur (1)
    - navigateurs (1)
    - responsive (2)
4. Revues de sources (total : 5)
    - propreté du code html (2.5)
    - propreté du code css (2.5)
1. La participation pendant les séances (2)
    - Votre présence (.25)
    - Votre interaction (profs / admin / étudiant / association) (.25)
    - ouverture issue [ici](https://gitlab.com/m2105/projet/issues)  (1.5)

## Les tâches à réaliser
---
Afin de vous aider à planifier votre travail, voici une liste de tâches (non exhaustives) que vous allez devoir mettre en œuvre :

1.	Traduire le cahier des charges pour imaginer à quoi va ressembler votre site (exemple de page, navigation…)
2.	Conception de la première page et écriture CSS
3.	Ecrire des contenus, validation de la navigation
4.	Test de vos pages, revu de votre code





